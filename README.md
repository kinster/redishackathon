# Quote Streams

Built for Redis Hackathon - Using Redis Server, Streams and Pub/Sub for real-time streaming of quotes.

## Getting Started

https://redis.io/download
```
./redis-server
```

```
cd ./quotes-api 
```
```
mvn spring-boot:run 
```
```
cd ./quotes-ui 
```
```
mvn spring-boot:run 
```

Quotes View
```
http://localhost:8080/?customerId=ac4ce8f9-80d4-4276-9603-fb9990bfed24 
```

Upload View
```
http://localhost:8080/upload?customerId=ac4ce8f9-80d4-4276-9603-fb9990bfed24
```

Example upload file is in quotes-ui/src/main/resources/csvsample.csv
