package com.flatcap.io;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class QuoteUiController {

    @Value("${quote.api.endpoint}")
    private String quoteEndpoint;

    @Value("${quote.path}")
    private String quotePath;

    @Value("${barcode.path}")
    private String barcodePath;

    @GetMapping
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("quoteEndpoint", quoteEndpoint);
        modelAndView.addObject("quotePath", quotePath);
        modelAndView.addObject("barcodePath", barcodePath);
        return modelAndView;
    }

    @GetMapping(value = "/upload")
    public ModelAndView upload() {
        ModelAndView modelAndView = new ModelAndView("upload");
        modelAndView.addObject("quoteEndpoint", quoteEndpoint);
        modelAndView.addObject("quotePath", quotePath);
        return modelAndView;
    }
}
