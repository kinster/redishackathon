package com.flatcap.io;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuoteUiApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuoteUiApplication.class, args);
    }

}
