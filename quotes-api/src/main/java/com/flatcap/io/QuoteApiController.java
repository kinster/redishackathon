package com.flatcap.io;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class QuoteApiController {

    @GetMapping
    public String index() {
        return "redirect:swagger-ui.html";
    }

}
