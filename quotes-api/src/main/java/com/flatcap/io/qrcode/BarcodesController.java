package com.flatcap.io.qrcode;

import net.glxn.qrgen.javase.QRCode;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

@RestController
@RequestMapping("/barcodes")
public class BarcodesController {

    @GetMapping(value = "/qrcode/{barcode}", produces = MediaType.IMAGE_PNG_VALUE)

    public ResponseEntity<byte[]> barbecueEAN13Barcode(@PathVariable("barcode") final String barcode) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(BarcodesController.generateQRCodeImage(barcode), "png", baos);
        HttpHeaders headers = new HttpHeaders();
        byte[] imageInByte = baos.toByteArray();
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(imageInByte, headers, HttpStatus.OK);
        return responseEntity;
    }

    public static BufferedImage generateQRCodeImage(final String barcodeText) throws Exception {
        ByteArrayOutputStream stream = QRCode.from(barcodeText).withSize(250, 250).stream();
        ByteArrayInputStream bis = new ByteArrayInputStream(stream.toByteArray());
        return ImageIO.read(bis);
    }

    @Bean
    public HttpMessageConverter<BufferedImage> createImageHttpMessageConverter() {
        return new BufferedImageHttpMessageConverter();
    }
}
