package com.flatcap.io.quote;

import com.flatcap.io.quote.Quote;
import org.springframework.data.repository.CrudRepository;

public interface QuoteRepository extends CrudRepository<Quote, String> {
}
