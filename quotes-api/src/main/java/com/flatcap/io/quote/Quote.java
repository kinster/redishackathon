package com.flatcap.io.quote;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;

@RedisHash(value = "quote", timeToLive = Quote.TWO_DAYS_IN_SECONDS)
@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
@JsonDeserialize
public class Quote implements Serializable {

    private static final long serialVersionUID = 7860573925617146529L;

    public static final long TWO_DAYS_IN_SECONDS = 172800L;

    public static final String KEY = "quote";

    @ApiModelProperty(example = "ac4ce8f9-80d4-4276-9603-fb9990bfed24")
    @Id
    private String id;
    @ApiModelProperty(example = "Toy car")
    private String name;
    @ApiModelProperty(example = "200g")
    private String weight;
    @ApiModelProperty(example = "£1.49")
    private String price;
    @ApiModelProperty(example = "Joey Smith")
    private String recipient;
    @ApiModelProperty(example = "BB2 6NG")
    private String postcode;

    private String date;

    @Indexed
    @ApiModelProperty(example = "CSV")
    private String source;

    @ApiModelProperty(example = "ac4ce8f9-80d4-4276-9603-fb9990bfed24")
    @Indexed
    private String customerId;

    private String barcode;
}
