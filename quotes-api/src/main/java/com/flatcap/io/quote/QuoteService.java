package com.flatcap.io.quote;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class QuoteService {

    private final QuoteReactiveRepository quoteReactiveRepository;

    public Flux<Quote> findByCustomerIdAndSource(final String customerId, final String source) {
        return quoteReactiveRepository.findByCustomerIdAndSource(customerId, source);
    }

    public Mono<Quote> save(final Quote quote) {
        return Mono.just(quote)
                .flatMap(quoteReactiveRepository::saveOrUpdate);
    }

}
