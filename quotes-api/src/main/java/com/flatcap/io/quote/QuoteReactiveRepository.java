package com.flatcap.io.quote;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
@RequiredArgsConstructor
@Slf4j
public class QuoteReactiveRepository {

    private final QuoteRepository quoteRepository;

    private final ReactiveRedisTemplate<String, String> reactiveRedisTemplate;

    public Flux<Quote> findByCustomerIdAndSource(final String customerId, final String source) {
        return reactiveRedisTemplate.opsForSet()
                .intersect(
                        Quote.KEY + ":customerId:" + customerId,
                        Quote.KEY + ":source:" + source)
                .map(quoteRepository::findById).flatMap(Mono::justOrEmpty);
    }

    public Mono<Quote> saveOrUpdate(final Quote quote) {
        log.info("saved: {}", quote);
        return Mono.just(quoteRepository.save(quote));
    }

}
