package com.flatcap.io.quote;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.ReactiveSubscription;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.ReactiveRedisMessageListenerContainer;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import javax.validation.Valid;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Stream;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping(value = "/quotes")
public class QuoteController {

    private final ReactiveRedisMessageListenerContainer reactiveMsgListenerContainer;
    private final ReactiveRedisOperations<String, Quote> redisOperations;
    private final QuoteService quoteService;
    private final QuoteReactiveRepository quoteReactiveRepository;
    private final ObjectMapper objectMapper;
    @Value("${quote.read.delay}")
    private long quoteReadDelay;

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Quote> get(@RequestParam("customerId") final String customerId, @RequestParam("source") final String source) {
        return Flux.<Quote>create(emitter ->
                quoteService.findByCustomerIdAndSource(customerId, source)
                        .concatWith(
                                receiveQuotes(customerId)
                        )
                        .subscribe(emitter::next))
                .delayElements(Duration.ofMillis(quoteReadDelay))
                .log()
                ;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Mono<Quote> post(@RequestBody @Valid final Quote quote) {
        return quoteService.save(quote)
                .flatMap(this::sendMessage);
    }

    @PostMapping("/csv")
    @ResponseBody
    public Flux<Quote> postOrders(@RequestParam("file") final MultipartFile f) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(f.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stream<Quote> quotes = br.lines().skip(1)
                .map(mapToQuote);

        return Flux.fromStream(quotes)
                .parallel()
                .runOn(Schedulers.elastic())
                .flatMap(this::post)
                .sequential()
                ;
    }

    private Flux<Quote> receiveQuotes(final String customerId) {
        log.info("Starting to receive  Messages from Channel {}", customerId);
        return reactiveMsgListenerContainer
                .receive(new ChannelTopic(customerId))
                .map(ReactiveSubscription.Message::getMessage)
                .map(msg -> {
                    try {
                        Quote value = objectMapper.readValue(msg, Quote.class);
                        log.info("New Message received: '" + value + "'.");
                        return value;
                    } catch (IOException e) {
                        log.error("Could not transform message {}", e);
                        return null;
                    }
                });
    }

    private Mono<Quote> sendMessage(final Quote quote) {
        log.info("sendMessage: " + quote.getCustomerId());
        return redisOperations.convertAndSend(quote.getCustomerId(), quote)
                .flatMap(q -> Mono.just(quote));
    }

    private Function<String, Quote> mapToQuote = (line) -> {
        String[] p = line.split(",");// a CSV has comma separated lines
        return Quote.builder()
                .name(p[0])
                .weight(p[1])
                .price(p[2])
                .recipient(p[3])
                .postcode(p[4])
                .source("CSV")
                .customerId(p[5])
                .date(LocalDateTime.now().toString())
                .barcode(this.generateBarcode())
                .build();
    };

    private String generateBarcode() {
        return UUID.randomUUID().toString();
    }
}
